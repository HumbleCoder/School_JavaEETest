package com.syc.utils;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * <bean id="dbUtils" class="com.syc.utils.DbUtils"/>
 */
@Component
/**
 * <aop:config>
 */
@Aspect
public class DbUtils {
    /**
     * <aop:pointcut id="cp" expression="execution(* com.syc.service.impl.DbServiceImpl.findData(*))"/>
     */
    @Pointcut("execution(* com.syc.service.impl.DbServiceImpl.findData(..))")
    public void pt(){}

    //<aop:before method="connectDb" pointcut-ref="cp"/>
    @Before("pt()")
    public void connectDb() {
        System.out.println("...连接数据库");
    }

    //<aop:after method="runDb" pointcut-ref="cp"/>
    @After("pt()")
    public void runDb() {
        System.out.println("...执行数据库");
    }

    //<aop:after-returning method="submitDb" pointcut-ref="cp"/>
    @AfterReturning("pt()")
    public void submitDb() {
        System.out.println("...数据提交");
    }

    //<aop:after-throwing method="rollbackDb" pointcut-ref="cp"/>
    @AfterThrowing("pt()")
    public void rollbackDb() {
        System.out.println("...数据回滚");
    }
}
