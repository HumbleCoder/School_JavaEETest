package com.syc.service.impl;

import com.syc.service.DbService;
import org.springframework.stereotype.Service;

/**
 * <bean id="dbService" class="com.syc.service.impl.DbServiceImpl"/>
 */
@Service
public class DbServiceImpl implements DbService {
    @Override
    public void findData(int n) {
        System.out.println("....执行SQL");
    }
}
