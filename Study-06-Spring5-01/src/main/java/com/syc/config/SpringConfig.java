package com.syc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
//<context:component-scan base-package="com.syc"/>
//<aop:aspectj-autoproxy/>

@Configuration
@ComponentScan("com.syc")
@EnableAspectJAutoProxy
public class SpringConfig {

}
