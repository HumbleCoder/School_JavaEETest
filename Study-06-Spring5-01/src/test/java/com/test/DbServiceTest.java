package com.test;

import com.syc.config.SpringConfig;
import com.syc.service.DbService;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DbServiceTest {
    @Test
    public void testDbService() {
        //ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        DbService dbService = context.getBean(DbService.class);
        dbService.findData(8);

    }
}
