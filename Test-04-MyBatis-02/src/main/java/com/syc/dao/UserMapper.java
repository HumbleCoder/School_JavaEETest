package com.syc.dao;

import com.syc.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {

    @Select("select * from user")
    List<User> findAllUser();

    @Select("select * from user where userid = #{id}")
    User findUserByID(@Param("id") int id);

    @Select("select * from user where ${column} like #{value}")
    User findUserByLike(@Param("column") String column, @Param("value") String value);

    @Insert("insert into book value(#{userid},#{username},#{birthday},#{sex},#{address})")
    int insertUser(User user);

    @Update("update user set ${column} = #{value} where userid = #{id}")
    int updateUserByID(@Param("id") int id, @Param("column") String column, @Param("value") String value);

    @Delete("delete from user where userid = #{id}")
    int deleteUserByID(@Param("id") int id);
}
