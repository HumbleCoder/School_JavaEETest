package com.syc.dao;

import com.syc.pojo.Book;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface BookMapper {

    @Select("select * from book")
    @Results(id = "bookMap", value = {
            @Result(id = true, column = "bookid", property = "bookid"),
            @Result(column = "bookname", property = "bookname"),
            @Result(column = "author", property = "author"),
            @Result(column = "publicationdate", property = "publicationdate"),
            @Result(column = "price", property = "price"),
            @Result(column = "userid", property = "user", one = @One(select = "com.syc.dao.UserMapper.findUserByID", fetchType = FetchType.EAGER)),
    })
    List<Book> findAllBook();

    @Select("select * from book where bookid = #{id}")
    @ResultMap("bookMap")
    Book findBookByID(@Param("id") int id);

    @Select("select * from book where ${column} like #{value}")
    @ResultMap("bookMap")
    Book findBookByLike(@Param("column") String column, @Param("value") String value);

    @Insert("insert into book value(#{bookid},#{bookname},#{author},#{publicationdate},#{price},#{user.userid})")
    int insertBook(Book book);

    @Update("update book set ${column} = #{value} where bookid = #{id}")
    int updateBookByID(@Param("id") int id, @Param("column") String column, @Param("value") String value);

    @Delete("delete from book where bookid = #{id}")
    int deleteBookByID(@Param("id") int id);
}
