package com.syc.test;

import com.syc.dao.BookMapper;
import com.syc.pojo.Book;
import com.syc.pojo.User;
import com.syc.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

public class MyBatisBookTest {
    static Logger logger = Logger.getLogger(MyBatisBookTest.class);
    SqlSession sqlSession = MyBatisUtils.getSqlSession();
    BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);

    @Test
    public void findAllBookTest() {
        List<Book> bookList = bookMapper.findAllBook();
        for (Book book : bookList
        ) {
            System.out.println(book);
        }
    }

    @Test
    public void findBookByIDTest() {
        Book book = bookMapper.findBookByID(5);
        System.out.println(book);
    }

    @Test
    public void findBookByLikeTest() {
        Book book = bookMapper.findBookByLike("bookname", "西%");
        System.out.println(book);
    }

    @Test
    public void insertBookTest() {
        bookMapper.insertBook(new Book(5,"聊斋","不知道",new Date(),56,new User()));
    }

    @Test
    public void updateBookByIDTest() {
        bookMapper.updateBookByID(5,"userid","28");
    }

    @Test
    public void deleteBookByIDTest() {
        bookMapper.deleteBookByID(5);
    }
}
