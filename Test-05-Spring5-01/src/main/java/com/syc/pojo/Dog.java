package com.syc.pojo;

import lombok.Data;

/**
 * 狗
 *
 * @author HumbleCoder
 * @date 2020/05/09
 */
@Data
public class Dog implements Pet {
    private String name;

    /**
     * 说
     */
    @Override
    public void say() {
        System.out.println("汪汪汪！！");
    }

    @Override
    public String toString() {
        return  name;
    }
}
