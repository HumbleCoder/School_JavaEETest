package com.syc.pojo;

/**
 * 宠物
 *
 * @author HumbleCoder
 * @date 2020/05/09
 */
public interface Pet {
    /**
     * 说
     */
    public void say();
}
