package com.syc.pojo;

import lombok.Data;

/**
 * 人
 *
 * @author HumbleCoder
 * @date 2020/05/09
 */
@Data
public class Person {
    private String name;
    private Pet pet;

    /**
     * 你好
     */
    public void hello(){
        System.out.println("Hello!My name is"+name+",I have a pet called "+pet);
        pet.say();
    }
}
