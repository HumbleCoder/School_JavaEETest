import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class reflect {
    public static void main(String[] args) throws
            ClassNotFoundException, NoSuchMethodException,
            IllegalAccessException, InvocationTargetException,
            InstantiationException, NoSuchFieldException {
//        Student stu = new Student("111","aaa",2);
//        System.out.println(stu.answer());

        Class stuClass = Class.forName("Student");//获取全称类名
        Constructor cons=stuClass.getConstructor(String.class,String.class,int.class);//构造构造函数
        Object obj =cons.newInstance("04171418","孙熠程",21);//实例化
        System.out.println(obj.toString());//这里不包括自定的方法，只要局限的几个

        //下面修改实例化对象的属性
        Field f =stuClass.getDeclaredField("age");
        f.setAccessible(true);
        f.set(obj,18);
        Method m = stuClass.getMethod("answer",null);//方法名
        System.out.println(m.invoke(obj,null));//反射调用
    }
}
