package com.syc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LoginAction {
    private String username;
    private String password;

    public String execute(){
        if (getUsername().equals("aaa") && getPassword().equals("bbb")) {
            return "success";
        }else{
            return "error";
        }
    }
}
