create database school;
use school;

CREATE TABLE `students` (
   `Sno` varchar(8) NOT NULL,
   `Sname` varchar(16) NOT NULL,
   `Ssex` varchar(8) NOT NULL DEFAULT '男',
   `Sbirthday` datetime DEFAULT NULL,
   `Sdept` varchar(45) DEFAULT NULL,
   `Memo` varchar(45) DEFAULT NULL,
   PRIMARY KEY (`Sno`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `courses` (
   `Cno` varchar(8) NOT NULL,
   `Cname` varchar(45) NOT NULL,
   `PreCno` varchar(45) DEFAULT NULL,
   `Credit` int(11) NOT NULL,
   PRIMARY KEY (`Cno`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sc` (
   `Sno` varchar(8) NOT NULL,
   `Cno` varchar(8) NOT NULL,
   `Grade` int(11) DEFAULT NULL,
   KEY `Sno-students_idx` (`Sno`),
   KEY `Cno-courses_idx` (`Cno`),
   CONSTRAINT `Cno-course` FOREIGN KEY (`Cno`) REFERENCES `courses` (`Cno`),
   CONSTRAINT `Sno-students` FOREIGN KEY (`Sno`) REFERENCES `students` (`Sno`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into Students values('0602001','钟耀华','男','1987-05-01','计算机','优秀毕业生');
insert into Students values('0602002','吴佳硕','女','1987-03-24','计算机','爱好:音乐');
insert into Students values('0602003','吴纯斌','男','1988-07-01','计算机',NULL);
insert into Students values('0701001','王腾飞','男','1988-05-04','机电','爱好:音乐');
insert into Students values('0701002','林师微','女','1987-04-03','机电','爱好:体育');
insert into Students values('0701003','李乐怡','女','1986-03-03','机电',NULL);
insert into Students values('0703001','李奇','男','1988-09-17','工商管理',NULL);

insert into Courses values('C1','数据结构',NULL,4);
insert into Courses values('C2','数据库原理','C1',4);
insert into Courses values('C3','大型数据库','C2',3);
insert into Courses values('C4','高尔夫',NULL,1);

insert into SC values('0602001','C1',61);
insert into SC values('0602001','C2',72);
insert into SC values('0602001','C3',88);
insert into SC values('0602002','C1',NULL);
insert into SC values('0602002','C2',61);
insert into SC values('0701001','C1',50);
insert into SC values('0701001','C2',NULL);
insert into SC values('0701002','C3',78);
insert into SC values('0701003','C1',52);
insert into SC values('0701003','C3',87);
