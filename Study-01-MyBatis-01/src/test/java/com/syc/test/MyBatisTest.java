package com.syc.test;

import com.syc.dao.StudentMapper;
import com.syc.pojo.Student;
import com.syc.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.util.List;

public class MyBatisTest {
    static Logger logger = Logger.getLogger(MyBatisTest.class);

    @Test
    public void findAllTest() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
        List<Student> studentList = studentMapper.findAll();
        for (Student student : studentList) {
            System.out.println(student);
        }
        sqlSession.close();
    }
}
