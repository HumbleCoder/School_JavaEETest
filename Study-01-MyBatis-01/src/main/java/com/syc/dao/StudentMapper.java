package com.syc.dao;

import com.syc.pojo.Student;

import java.util.List;

public interface StudentMapper {
    List<Student> findAll();
}
