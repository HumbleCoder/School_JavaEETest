package com.syc.pojo;

import java.io.Serializable;
import java.util.Date;

public class Student implements Serializable {
    private String Sno;
    private String Sname;
    private String Ssex;
    private Date Sbirthday;
    private String Sdept;
    private String Memo;

    public Student() {
    }

    public Student(String sno, String sname, String ssex, Date sbirthday, String sdept, String memo) {
        Sno = sno;
        Sname = sname;
        Ssex = ssex;
        Sbirthday = sbirthday;
        Sdept = sdept;
        Memo = memo;
    }

    public String getSno() {
        return Sno;
    }

    public void setSno(String sno) {
        Sno = sno;
    }

    public String getSname() {
        return Sname;
    }

    public void setSname(String sname) {
        Sname = sname;
    }

    public String getSsex() {
        return Ssex;
    }

    public void setSsex(String ssex) {
        Ssex = ssex;
    }

    public Date getSbirthday() {
        return Sbirthday;
    }

    public void setSbirthday(Date sbirthday) {
        Sbirthday = sbirthday;
    }

    public String getSdept() {
        return Sdept;
    }

    public void setSdept(String sdept) {
        Sdept = sdept;
    }

    public String getMemo() {
        return Memo;
    }

    public void setMemo(String memo) {
        Memo = memo;
    }

    @Override
    public String toString() {
        return "Student{" +
                "Sno='" + Sno + '\'' +
                ", Sname='" + Sname + '\'' +
                ", Ssex='" + Ssex + '\'' +
                ", Sbirthday=" + Sbirthday +
                ", Sdept='" + Sdept + '\'' +
                ", Memo='" + Memo + '\'' +
                '}';
    }
}
