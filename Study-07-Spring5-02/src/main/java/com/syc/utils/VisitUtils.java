package com.syc.utils;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class VisitUtils {
    @Pointcut("execution(* com.syc.service.VisitServiceImpl.visit(*))")
    public void pt(){}

    @Before("pt()")
    public void before() {
        System.out.print("口令:");
    }

    @After("pt()")
    public void after() {
        System.out.println("输入完成");
    }

    @AfterReturning("pt()")
    public void afterReturning() {
        System.out.println("请求成功，欢迎");
    }

    @AfterThrowing("pt()")
    public void afterThrowing() {
        System.out.println("请求失败，拒绝");
    }

}
