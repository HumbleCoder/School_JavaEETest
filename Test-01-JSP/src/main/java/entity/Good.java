package entity;

public class Good {
    private double price;
    private int number;

    public Good(double price, int number) {
        this.price = price;
        this.number = number;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double calcsum(){
        double dis = 0;
        if(0<number&&number<100){
            dis = 0;
        }else if(number<300){
            dis = 0.05;
        }else if(number<1000){
            dis = 0.1;
        }else{
            dis = 0.15;
        }
        return price*number*(1-dis);
    }
}
