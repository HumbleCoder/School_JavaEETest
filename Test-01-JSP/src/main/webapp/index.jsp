<%--
  Created by IntelliJ IDEA.
  User: Max Brain
  Date: 2020/4/14
  Time: 13:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
    <title>计算</title>
</head>
<body>
<div class="container">
    <h1 class="page-header">计算格</h1>
    <form action="Sum" method="post" name="form" class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-md-1" style="font-size:larger;" >价格</label>
            <div class="col-md-3">
                <input name="price" type="text" id="price" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-1" style="font-size: larger;" >数量</label>
            <div class="col-md-3">
                <input name="number" type="text" id="number" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-1 col-md-3">
                <input name="submit" type="submit" id="submit" value="提交" class="btn btn-success btn-block">
            </div>
        </div>
    </form>
</div>
</body>
</html>
