package com.syc.service;

import org.springframework.stereotype.Service;

//<bean id="visitService" class="com.syc.service.VisitServiceImpl"/>
@Service
public class VisitServiceImpl implements VisitService{
    @Override
    public void visit(String str) throws Exception {
        System.out.println(str);
        if(!str.equalsIgnoreCase("agree")){
            throw new Exception("非法访问");
        }
    }
}
