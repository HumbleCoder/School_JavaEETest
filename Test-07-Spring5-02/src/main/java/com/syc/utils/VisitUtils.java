package com.syc.utils;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

//<bean id="visitUtils" class="com.syc.utils.VisitUtils"/>
//<aop:config>
@Component
@Aspect
public class VisitUtils {
    //<aop:pointcut id="cp" expression="execution(* com.syc.service.VisitServiceImpl.visit(*))"/>
    @Pointcut("execution(* com.syc.service.VisitServiceImpl.visit(*))")
    public void pt(){}

    //<aop:before method="before" pointcut-ref="cp"/>
    @Before("pt()")
    public void before() {
        System.out.print("口令：");
    }

    //<aop:after method="after" pointcut-ref="cp"/>
    @After("pt()")
    public void after() {
        System.out.println("输入完成");
    }

    //<aop:after-returning method="afterReturning" pointcut-ref="cp"/>
    @AfterReturning("pt()")
    public void afterReturning() {
        System.out.println("请求成功，欢迎");
    }

    //<aop:after-throwing method="afterThrowing" pointcut-ref="cp"/>
    @AfterThrowing("pt()")
    public void afterThrowing() {
        System.out.println("请求失败，拒绝");
    }
}
