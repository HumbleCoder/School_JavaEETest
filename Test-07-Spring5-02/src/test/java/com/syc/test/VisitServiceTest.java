package com.syc.test;

import com.syc.config.SpringConfiguration;
import com.syc.service.VisitService;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class VisitServiceTest {
    @Test
    public void visitServiceTest() {
        //ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        VisitService visitService = context.getBean(VisitService.class);

        try {
            visitService.visit("agree");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            visitService.visit("ok");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
