package com.syc.test;

import com.syc.dao.BookMapper;
import com.syc.pojo.Book;
import com.syc.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

public class MyBatisTest {

    static Logger logger = Logger.getLogger(MyBatisTest.class);

    @Test
    public void getAllTest(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
        List<Book> bookList = bookMapper.getAll();

        for (Book book:bookList
        ) {
            System.out.println(book);
        }
    }

    @Test
    public void updateByIdTest(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
        bookMapper.updateById(new Book(5,"hahah","syc",new Date(),12));
    }

    @Test
    public void insertBookTest(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
        bookMapper.insertBook(new Book(5,"聊斋","不知道",new Date(),12));
    }

    @Test
    public void deleteByIdTest(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
        bookMapper.deleteById(5);
    }
}
