package com.syc.dao;

import com.syc.pojo.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface BookMapper {
    //@Select("select * from book")
    List<Book> getAll();

    //@Update("update book set bookname=#{bookname},author = #{author},publicationdate = #{publicationdate},price = #{price} where id = #{id}")
    int updateById(Book book);

    //@Insert("insert into book(id,bookname,author,publicationdate,price) value(#{id},#{bookname},#{author},#{publicationdate},#{price})")
    int insertBook(Book book);

    //@Delete("delete from book where id = #{id}")
    int deleteById(@Param("id") int id);

}
