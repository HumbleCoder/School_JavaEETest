CREATE DATABASE `hellomybatis`;
use hellomybatis;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookname` varchar(45) NOT NULL,
  `author` varchar(45) NOT NULL,
  `publicationdate` datetime NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `book` VALUES
(1,'西游记','吴承恩','1980-02-27 17:47:08',65),
(2,'三国演义','罗贯中','1991-03-02 15:09:37',59),
(3,'水浒传','施耐庵','1984-05-02 17:29:33',61),
(4,'红楼梦','曹雪芹','1994-06-06 21:33:18',88);
