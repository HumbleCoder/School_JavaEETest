package com.syc.dao;

import com.syc.pojo.Book;
import com.syc.pojo.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface UserMapper {
    @Select("select * from user")
    List<Book> findAllUser();

    @Select("select * from user where userid = #{id}")
    User findUserByID(@Param("id") int id);

    @Insert("insert into user value(#{userid},#{username},#{birthday},#{sex},#{address})")
    int insertUser(User user);

    @Update("update user set ${column}=#{value} where userid = #{id}")
    int updateUserByID(@Param("id") int id,@Param("column") String column,@Param("value") String value);

    @Delete("delete from book where userid = #{id}")
    int deleteUserByID(@Param("id") int id);
}
