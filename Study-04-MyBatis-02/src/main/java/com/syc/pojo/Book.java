package com.syc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Book implements Serializable {
    private int bookid;
    private String bookname;
    private String author;
    private Date publicationdate;
    private double price;

    private User user;

    public Book(String bookname, String author, Date publicationdate, double price, User user) {
        this.bookname = bookname;
        this.author = author;
        this.publicationdate = publicationdate;
        this.price = price;
        this.user = user;
    }
}
