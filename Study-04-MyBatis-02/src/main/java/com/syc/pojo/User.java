package com.syc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class User implements Serializable {
    private int userid;
    private String username;
    private Date birthday;
    private String sex;
    private String address;

    public User() {
        this.userid = 0;
    }
}
