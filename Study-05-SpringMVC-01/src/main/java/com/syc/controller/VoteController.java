package com.syc.controller;

import com.syc.pojo.Vote;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


/**
 * 投票控制器
 *
 * @author HumbleCoder
 * @date 2020/05/12
 */
@Controller
public class VoteController {

    @RequestMapping("/vote")
    public ModelAndView vote(@Valid Vote vote, Errors errors, ModelAndView mv){
        if(errors.hasErrors()){
            mv.addObject("Error","你年龄错了,请重新输入");
            mv.setViewName("error");
            return mv;
        }else{
            if(vote.getName().equals("尼格尔")){
                vote.setUrl("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3713885119,3047340312&fm=11&gp=0.jpg");
            }else if(vote.getName().equals("罗志祥")){
                vote.setUrl("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=611078855,597734248&fm=11&gp=0.jpg");
            }else if(vote.getName().equals("特朗普")){
                vote.setUrl("https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=4119698188,1828122786&fm=26&gp=0.jpg");
            }else if(vote.getName().equals("孙笑川")){
                vote.setUrl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1242360552,3066555732&fm=26&gp=0.jpg");
            }else{
                return null;
            }
            mv.addObject("name",vote.getName());
            mv.addObject("age",vote.getAge());
            mv.addObject("url",vote.getUrl());
            mv.setViewName("success");
            return mv;
        }
    }
}
