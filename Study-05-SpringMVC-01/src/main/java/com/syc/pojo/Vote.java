package com.syc.pojo;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 投票
 *
 * @author HumbleCoder
 * @date 2020/05/12
 */
@Data
public class Vote {
    @NotBlank
    private String name;
    @NotNull
    @Range(min=0,max=100)
    private int age;
    private String url;
}
