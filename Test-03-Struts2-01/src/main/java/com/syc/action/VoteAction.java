package com.syc.action;

import com.opensymphony.xwork2.Action;
import com.syc.pojo.Vote;
import lombok.Data;

@Data
public class VoteAction implements Action {

    private Vote vote;

    @Override
    public String execute() throws Exception {
        if(vote.getName().equals("尼格尔")){
            vote.setUrl("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3713885119,3047340312&fm=11&gp=0.jpg");
            return "success";
        }else if(vote.getName().equals("罗志祥")){
            vote.setUrl("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=611078855,597734248&fm=11&gp=0.jpg");
            return "success";
        }else if(vote.getName().equals("特朗普")){
            vote.setUrl("https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=4119698188,1828122786&fm=26&gp=0.jpg");
            return "success";
        }else if(vote.getName().equals("孙笑川")){
            vote.setUrl("https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=1242360552,3066555732&fm=26&gp=0.jpg");
            return "success";
        }else{
            return null;
        }
    }
}
