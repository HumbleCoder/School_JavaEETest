<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Max Brain
  Date: 2020/4/29
  Time: 20:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>你的本名英雄</title>
</head>
<body>
<h2>你选择的本名英雄是：<s:property value="vote.name"/></h2>
<img src="<s:property value="vote.url"/>">
</body>
</html>
