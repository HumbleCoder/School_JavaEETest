<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <h2>请选择你的本命英雄</h2>
    <form action="vote" method="post">
        <fieldset class="form-group">
            <div class="row">
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios1" value="尼格尔" checked>
                        <label class="form-check-label" for="gridRadios1">
                            黑人抬棺，专业团队
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios2" value="罗志祥">
                        <label class="form-check-label" for="gridRadios2">
                            你见过凌晨四点的罗志祥吗？
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios3" value="特朗普">
                        <label class="form-check-label" for="gridRadios3">
                            美国历史上最伟大的总统川建国
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios4" value="孙笑川">
                        <label class="form-check-label" for="gridRadios4">
                            儒雅随和大师兄
                        </label>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-success">我选择好了</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>
