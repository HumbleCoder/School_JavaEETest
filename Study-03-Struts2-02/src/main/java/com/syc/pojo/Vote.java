package com.syc.pojo;

import lombok.Data;

@Data
public class Vote {
    private String name;
    private String url;
}
