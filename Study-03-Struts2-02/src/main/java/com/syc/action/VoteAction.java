package com.syc.action;

import com.opensymphony.xwork2.Action;
import com.syc.pojo.Vote;
import lombok.Data;

@Data
public class VoteAction implements Action {

    private Vote vote;

    @Override
    public String execute() throws Exception {
        if(vote.getName().equals("尼格尔")){
            vote.setUrl("");
            return "success";
        }else if(vote.getName().equals("罗志祥")){
            vote.setUrl("");
            return "success";
        }else if(vote.getName().equals("特朗普")){
            vote.setUrl("");
            return "success";
        }else if(vote.getName().equals("孙笑川")){
            vote.setUrl("");
            return "success";
        }else{
            return null;
        }
    }
}
