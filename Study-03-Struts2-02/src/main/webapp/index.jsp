<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <h2>请选择你的本命英雄</h2>
    <form action="vote" method="post">
        <fieldset class="form-group">
            <div class="row">
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios1" value="尼格尔"
                               checked>
                        <label class="form-check-label" for="gridRadios1">
                            黑人抬棺，专业团队
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios2" value="罗志祥">
                        <label class="form-check-label" for="gridRadios2">
                            你见过凌晨四点的罗志祥吗?
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios3" value="特朗普">
                        <label class="form-check-label" for="gridRadios2">
                            美国历史上最伟大的总统川建国
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="vote.name" id="gridRadios4" value="孙笑川">
                        <label class="form-check-label" for="gridRadios2">
                            儒雅随和大师兄
                        </label>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-success">我选择好了</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>
