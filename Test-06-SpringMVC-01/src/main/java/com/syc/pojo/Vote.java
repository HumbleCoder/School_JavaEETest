package com.syc.pojo;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * 投票
 *
 * @author HumbleCoder
 * @date 2020/05/13
 */
@Data
public class Vote {
    /**
     * 名字
     */
    @NotNull
    private String name;
    /**
     * 年龄
     */
    @NotNull
    @Range(min=0,max=99)
    private int age;
    /**
     * url
     */
    private String url;
}
